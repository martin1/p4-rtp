## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268 paquetes
* ¿Cuánto tiempo dura la captura?: 12,81 segundos
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 100 paquetes/segundo aproximadamente
* ¿Qué protocolos de nivel de red aparecen en la captura?: IP
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 109
* Dirección IP de la máquina A: 216.234.64.16
* Dirección IP de la máquina B: 192.168.0.10
* Puerto UDP desde el que se envía el paquete: 54550
* Puerto UDP hacia el que se envía el paquete:49154
* SSRC del paquete: 0x31be1e0e (834543118) es el SSRC del paquete
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU (0)

## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 5
* ¿Cuántos paquetes hay en el flujo F?: 642 paquetes en el flujo F
* ¿El origen del flujo F es la máquina A o B?: La máquina A
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550
* ¿Cuántos segundos dura el flujo F?: 12,81 segundos 
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 21,187 ms
* ¿Cuál es el jitter medio del flujo?: 0,229065 segundos es el jitter medio del flujo 
* ¿Cuántos paquetes del flujo se han perdido?: 0 paquetes perdidos
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 0,5 segundos
## Ejercicio 7. Métricas de un paquete RTP (Escogeré el paquete número 110)
* ¿Cuál es su número de secuencia dentro del flujo RTP?:92119 es el número de secuencia
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?:Este paquete ha llegado pronto con respecto a cuando habría debido llegar. Exactamente 9.825 ms antes.
* ¿Qué jitter se ha calculado para ese paquete? 11.989135 ms
* ¿Qué timestamp tiene ese paquete? 8800
* ¿Por qué número hexadecimal empieza sus datos de audio? 7d7bfefefd7bfefc7cfd7bfefc7cfc7d7bfdfc7d7cfd7bfefefd7bfefefc7cfc7d7cfd7b…

## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: vie 20 oct 2023 10:06:12 CEST
* Número total de paquetes en la captura: 4090 paquetes en la captura
* Duración total de la captura (en segundos): 75.180545 segundos
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450 paquetes RTP
* ¿Cuál es el SSRC del flujo?: 0x6c0f1a0
* ¿En qué momento (en ms) de la traza comienza el flujo? 71975.723 ms
* ¿Qué número de secuencia tiene el primer paquete del flujo? 26272
* ¿Cuál es el jitter medio del flujo?: 0 es el jitter medio del flujo
* ¿Cuántos paquetes del flujo se han perdido?: 0
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: ddez
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): Mi compañero tiene 2580 paquetes más.
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: Tenemos el mismo número de paquetes RTP (450 paquetes).
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: 5267 es la diferencia entre el número de secuencia del primer paquete de su flujo RTP con respecto al mío.
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: El puerto de origen es distinto. El SSRC es diferente. El start time del flujo también es distinto. El mean delta varía unos pocos decimales. Por lo demás, los flujos tienen las mismas características: mismo número de paquetes RTP, mismo jitter medio, mismo número de paquetes perdidos
